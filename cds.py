#!/usr/bin/env python3

from __future__ import annotations
import datetime
import json
import pathlib
import re
import urllib.parse
import urllib.request

import awkward

'''https://cds.cern.ch/help/hacking/search-engine-api'''

def search(query: str = '', **kwargs) -> awkward.Array:
    params = urllib.parse.urlencode(query={'p': query, 'of': 'recjson', **kwargs})
    url = f'https://cds.cern.ch/search?{params}'
    response = urllib.request.urlopen(url).read().decode('utf-8')
    return awkward.from_json(response) if response else awkward.Array([])

def issues(year: int):
    print(year)
    cc = search(cc='CERN Courier Issues', rg=100, f1='year', p1=year)
    if cc.fields:
        pdf_files = awkward.flatten(cc.files[cc.files.eformat == '.pdf'])
        pdf_full_name = [re.sub(pattern='(\d+)', repl=lambda match: f'{int(match.group(1)):02d}', string=filename) + f'-{lang}-{recid}.pdf' for filename, recid, lang in zip(pdf_files.name, cc.recid, cc.language)] # https://stackoverflow.com/a/56723200
        return list(zip(pdf_files.url, pdf_full_name))

def issues_per_year():
    all_issues = {str(year): issues(year) for year in range(1959, datetime.datetime.now().year+1)}
    json.dump(obj=all_issues, fp=open('cern_courier_issues.json', mode='w'))

def download_pdf(url: str, filepath: pathlib.Path):
    print(filepath)
    URL = urllib.parse.urlparse(url)
    url = f'{URL.scheme}://{URL.netloc}/{urllib.parse.quote(URL.path)}'
    with filepath.open(mode='wb') as pdf_file:
        pdf_file.write(urllib.request.urlopen(url).read())

def download_year(year: str, issues: list[list(str)]):
    if not issues:
        return
    pathlib.Path(str(year)).mkdir(exist_ok=True)
    for url, filename in issues:
        download_pdf(url=url, filepath=pathlib.Path(f'{year}/{filename}'))

def download_issues(begin_year: int = None):
    if not pathlib.Path('cern_courier_issues.json').exists():
        raise ValueError('run `issues_per_year` function first.')
    with open('cern_courier_issues.json', mode='r') as issues_json:
        issues = json.load(issues_json)
    if begin_year:
        _ = [issues.pop(str(year)) for year in range(1959, begin_year)]
    _ = [download_year(year, issues) for year, issues in issues.items()]

if __name__ == '__main__':
    issues_per_year()
    download_issues()
